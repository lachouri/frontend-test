import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import MainPage from "./components/MainPage";
import FieldComponent from "./components/FieldComponent";

class App extends React.Component {
  render() {
    return (
      <BrowserRouter basename="/">
        <div className="app">
          <Switch>
            <Route exact path="/" component={MainPage} />
            <Route exact path="/field" component={FieldComponent} />
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
