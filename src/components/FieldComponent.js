import React, { useState } from "react";

const EditableField = ({ name = "Name", value = "Charlie Chaplin" }) => {
  const [mode, setMode] = useState("readonly");
  console.log(mode);
  const handleOnClick = (e) => {
    e.preventDefault();
    setMode(false);
  };

  const handleOnSubmit = (e) => {
    e.preventDefault();
    setMode("readonly");
  };

  return (
    <form class="w3-container">
      <label>{name}</label>
      <input
        class="input"
        type="text"
        value={value}
        readonly={mode}
        onClick={handleOnClick}
      />
      <submit type="button" onClick={handleOnSubmit}>
        Validate
      </submit>
    </form>
  );
};

export default EditableField;
