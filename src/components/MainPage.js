import React, { useState } from "react";
import ListItem from "./ListItem";
import MainContentBlock from "./MainCenterBlock";
import { listItems } from "../models/api";
import RightDetailPanel from "./RightDetailPanel";

const MainPage = () => {
  const [data, setData] = useState(null);
  listItems().then((data) => setData(data.data));
  const [details, setDetails] = useState(null);
  const handleOnClick = (data) => {
    const details = <RightDetailPanel id={data} />;
    return setDetails(details);
  };
  const anElement = data?.map((aData) => (
    <ListItem
      id={aData.id}
      name={aData.name}
      onClick={(e) => {
        handleOnClick(e);
      }}
    />
  ));
  return (
    <>
      <MainContentBlock>{anElement}</MainContentBlock>
      {details ? details : null}
    </>
  );
};

export default MainPage;
