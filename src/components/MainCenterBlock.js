import React from "react";

const MainContentBlock = ({ children }) => {
  return <>{children ? children : null}</>;
};

export default MainContentBlock;
