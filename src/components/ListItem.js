import PropTypes from "prop-types";

const ListItem = ({ id, name, onClick }) => {
  const handleOnClick = (e) => {
    onClick(e);
  };
  return (
    <div>
      <button key={id} onClick={() => handleOnClick(id)}>
        {name}
      </button>
    </div>
  );
};

ListItem.propTypes = {
  name: PropTypes.string,
};

export default ListItem;
