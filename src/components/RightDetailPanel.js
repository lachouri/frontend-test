import React, { useState } from "react";
import { getItem } from "../models/api";

const RightDetailPanel = ({ id }) => {
  const [data, setData] = useState(null);
  getItem(id).then((data) => setData(data));
  console.log("data", data);

  if (data) {
    return (
      <div style={{ float: "right" }}>
        <p>Name: {data.name}</p>
        <p>Field 1: {data.details.field1}</p>
        <p>Field 2: {data.details.field2.toString()}</p>
        <p>Field 3: {data.details.field3}</p>
      </div>
    );
  }

  return null;
};

export default RightDetailPanel;
